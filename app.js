var express = require("express"),
    envData = require('dotenv').config(),
    nodemailer = require("nodemailer"),
    bodyParser = require("body-parser"),
    { Client } = require('pg'), 
    dateFormat = require("dateformat"),
    flash = require("connect-flash"),
    path = require("path"),
    session = require('express-session'),
    pGSession = require("connect-pg-simple")(session),
    cookieParser = require('cookie-parser'),
    app = express();
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));
var connectionString = process.env.DATABASE_URL;

//Flashing
app.use(cookieParser());
app.use(session({
    store: new pGSession({
        conString: connectionString
    }),
    secret: "SESSION_SECRET",
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 30 * 24 * 60 * 60 * 1000}
}));
app.use(flash());

app.get("/public/stylesheets/main.css", function(req, res) {
    res.render(__dirname + '/public/stylesheets/main.css');
});
app.get("/", function(req, res) {
    res.render("landing.ejs");
});

app.get("/about", function(req, res) {
    res.render("about.ejs");
});

app.get("/blog", function(req, res) {
 
    const client = new Client({
        connectionString: connectionString,
        ssl: true,
    });

    client.connect();
    var query = "SELECT posts.title, posts.created, posts.rating, posts.id, \
    authors.name, posts.picture FROM blogpostsdb.posts INNER JOIN blogpostsdb.authors ON posts.author = \
    authors.id ORDER BY posts.created";
    client.query(query, function(err, queryResult){
        if (err) console.log(err)
        else {
            var postList = [];
            for (var i=0; i<queryResult.rows.length; i++) {
                var post = {
                    'title':queryResult.rows[i].title,
                    'id':queryResult.rows[i].id,
                    'created':dateFormat(queryResult.rows[i].created, "yyyy-mm-dd"),
                    'rating':queryResult.rows[i].rating,
                    'name':queryResult.rows[i].name,
                    'picture':queryResult.rows[i].picture
                }
                postList.push(post);
            }
            var tag = false;
            res.render("index.ejs", {"postList" : postList, "tag" : tag});
        }
        client.end();
    });

});

app.get("/show/:id", function(req, res) {
    
    const client = new Client({
        connectionString: connectionString,
        ssl: true,
    });
    
    client.connect();

    var query = "SELECT posts.title, posts.created, posts.rating, authors.name, \
    posts.note, posts.quote, posts.created, posts.picture, posts.id from blogpostsdb.posts \
    INNER JOIN blogpostsdb.authors ON posts.author=authors.id WHERE posts.id =\
    '" + req.params.id + "'";

    client.query(query, function(err, queryResult) { 
        if (err) console.log(err)
        else {
			var postInfo;
			postInfo = {
                'title':queryResult.rows[0].title,
                'created':dateFormat(queryResult.rows[0].created, "yyyy-mm-dd"),
                'rating':queryResult.rows[0].rating,
                'name':queryResult.rows[0].name,
                'note':queryResult.rows[0].note,
                'quote':queryResult.rows[0].quote,
                'picture':queryResult.rows[0].picture,
                'id': queryResult.rows[0].id
            }
            var query2 = "SELECT tags.tag FROM blogpostsdb.posts INNER JOIN \
            blogpostsdb.tags ON tags.title=\
            posts.id WHERE posts.id='" + req.params.id + "'";
    
            const client2 = new Client({
                connectionString: connectionString,
                ssl: true,
            });

            client2.connect();

            client2.query(query2, function(err, queryResult) {
                if (err) console.log(err)
                else {
                    var tagsList = [];
                    for (var i=0; i<queryResult.rows.length; i++) {
                        tagsList.push(queryResult.rows[i].tag);
                    }
                    res.render("show.ejs", {"postInfo": postInfo, "tagsList" :tagsList});
                }
                client2.end();
            });
        }
        client.end();
    });
});

app.get("/show/tags/:tag", function(req, res) {
   
    const client = new Client({
        connectionString: connectionString,
        ssl: true,
    });
    
    client.connect();
    var query = "SELECT posts.title, posts.created, posts.rating, posts.id, \
    posts.picture, authors.name FROM blogpostsdb.posts INNER JOIN blogpostsdb.authors ON posts.author = authors.id \
    INNER JOIN blogpostsdb.tags ON tags.title = posts.id WHERE tags.tag = '" + req.params.tag + "' \
    ORDER BY posts.created";

    client.query(query, function(err, queryResult) {
        if (err) console.log(err)
        else {
            var postList = [];
            for (var i=0; i<queryResult.rows.length; i++) {
                var post = {
                    'title':queryResult.rows[i].title,
                    'id':queryResult.rows[i].id,
                    'created':dateFormat(queryResult.rows[i].created, "yyyy-mm-dd"),
                    'rating':queryResult.rows[i].rating,
                    'name':queryResult.rows[i].name,
                    'picture':queryResult.rows[i].picture
                }
                postList.push(post);
            }
            var tag = req.params.tag;
            res.render("index.ejs", {"postList" : postList, "tag" : tag});
        }
    client.end();
    });
});

app.get("/contact", function(req, res) {

    res.render("contact.ejs", { message: req.flash("success")});
});

app.post("/contact", function(req, res) {
    let mailOpts, smtpTrans;
    smtpTrans = nodemailer.createTransport({
        host: process.env.SMTPHOST,
        port: process.env.SMTPPORT,
        secure: true,
        auth: {
            user: process.env.SMTPAUTHUSER ,
            pass: process.env.SMTPAUTHPASS 
        }
    });
    mailOpts = {
        from: req.body.name,
        to: process.env.MAILTO ,
        subject: "New message from the contact form",
        text: req.body.message + " from " + req.body.email
    };
    smtpTrans.sendMail(mailOpts, function (error, response) {
        if (error) {
            console.log(error);
        } else {
            req.flash("success", "The message has been sent. Thank you!")
            res.redirect("/contact");
        }
    });
});

app.get("/portfolio", function(req, res) {
    res.render("portfolio.ejs");
});

app.get("/cv", function(req, res) {
    res.render("CV.ejs");
});

app.listen(process.env.PORT, function() {
    console.log("the server is listening...");
});
